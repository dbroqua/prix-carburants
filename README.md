Ce projet a simplement pour but de répertorier l'ensemble des stations essences en France.

Les données sont basées sur les [Open Data](https://www.prix-carburants.gouv.fr/rubrique/opendata/).

# Utilisation

Une fois le projet cloné vous devrez commencer par créer un fichier `.env` qui contiendra des variables d'environnement :

```
REACT_APP_MAPBOX_TOKEN=********
REACT_APP_API_URL=********
REACT_APP_MAP_RADIUS=20000

```

Une fois fait :

```bash
yarn install
yarn start
```